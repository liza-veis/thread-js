const DEFAULT_USER_AVATAR = 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png';
const USER_IMAGE_MAX_SIZE = 200;

export { DEFAULT_USER_AVATAR, USER_IMAGE_MAX_SIZE };
