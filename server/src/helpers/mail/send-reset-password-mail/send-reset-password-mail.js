import mailer from '@sendgrid/mail';
import { ENV } from '../../../common/enums/enums';

const {
  EMAIL
} = ENV.SENDGRID;

const sendResetPasswordMail = ({ to, link }) => (
  mailer.send({
    to,
    from: EMAIL,
    subject: 'Password Recovery',
    html: `<p>Click to set a new password: <a href="${link}">Reset password</a></p>`
  })
);

export { sendResetPasswordMail };
