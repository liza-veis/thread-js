import mailer from '@sendgrid/mail';
import { ENV } from '../common/enums/enums';

const {
  API_KEY
} = ENV.SENDGRID;

mailer.setApiKey(API_KEY);

