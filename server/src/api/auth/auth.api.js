import { AuthApiPath } from '../../common/enums/enums';
import {
  authentication as authenticationMiddleware,
  forgotPasswordAuthentication as forgotPasswordMiddleware,
  resetPasswordAuthentication as resetPasswordMiddleware,
  registration as registrationMiddleware,
  jwt as jwtMiddleware
} from '../../middlewares/middlewares';

const initAuth = (Router, services) => {
  const { auth: authService, user: userService } = services;
  const router = Router();

  // user added to the request (req.user) in a strategy, see passport config
  router
    .post(AuthApiPath.LOGIN, authenticationMiddleware, (req, res, next) => authService
      .login(req.user)
      .then(data => res.send(data))
      .catch(next))
    .post(AuthApiPath.REGISTER, registrationMiddleware, (req, res, next) => authService
      .register(req.user)
      .then(data => res.send(data))
      .catch(next))
    .post(AuthApiPath.FORGOT, forgotPasswordMiddleware, (req, res, next) => authService
      .requestResetPassword(req.body)
      .then(data => res.send(data))
      .catch(next))
    .put(AuthApiPath.RESET, resetPasswordMiddleware, (req, res, next) => authService
      .resetPassword(req.params.userId, req.body)
      .then(data => res.send(data))
      .catch(next))
    .get(AuthApiPath.USER, jwtMiddleware, (req, res, next) => userService
      .getUserById(req.user.id)
      .then(data => res.send(data))
      .catch(next))
    .put(AuthApiPath.USER, jwtMiddleware, (req, res, next) => userService
      .update(req.user.id, req.body)
      .then(data => res.send(data))
      .catch(next));

  return router;
};

export { initAuth };
