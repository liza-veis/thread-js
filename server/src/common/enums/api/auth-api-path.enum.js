const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  FORGOT: '/forgot',
  RESET: '/reset/:userId/:token'
};

export { AuthApiPath };
