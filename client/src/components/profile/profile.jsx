import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ReactCrop from 'react-image-crop';
import clsx from 'clsx';
import { image as imageService, user as userService } from 'src/services/services';
import { profileActionCreator, threadActionCreator } from 'src/store/actions';
import { Grid, Button, Form, Image, Message } from 'src/components/common/common';
import { IconName, ButtonType, ButtonColor } from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR, USER_IMAGE_MAX_SIZE } from 'src/common/constants/constants';
import { getCroppedImage } from './helpers/helpers';

import 'react-image-crop/lib/ReactCrop.scss';
import styles from './styles.module.scss';

const initialCrop = {
  width: USER_IMAGE_MAX_SIZE,
  height: USER_IMAGE_MAX_SIZE,
  x: 0,
  y: 0
};

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const [isEdited, setIsEdited] = React.useState(false);
  const [username, setUsername] = React.useState(user?.username);
  const [status, setStatus] = React.useState(user?.status);
  const [image, setImage] = React.useState(user?.image);
  const [crop, setCrop] = React.useState(initialCrop);
  const [isUploading, setIsUploading] = React.useState(false);
  const [isUsernameError, setIsUsernameError] = React.useState(false);
  const [isImageError, setIsImageError] = React.useState(false);
  const showCrop = isEdited && image?.id !== user.image?.id;
  const dispatch = useDispatch();

  const uploadImage = file => imageService.uploadImage(file);

  const getUserByUsername = () => userService.getUserByUsername(username);

  const updateCurrentUser = data => (
    dispatch(profileActionCreator.updateCurrentUser(data))
  );

  const loadUserPosts = () => (
    dispatch(threadActionCreator.loadUserPosts(user.id))
  );

  const handleIsEditedToggle = () => {
    setIsEdited(!isEdited);
    setCrop(initialCrop);
  };

  const handleUsernameChange = ev => {
    setUsername(ev.target.value);
    setIsUsernameError(false);
  };

  const handleStatusChange = ev => {
    setStatus(ev.target.value);
  };

  const handleUploadFile = ({ target }) => {
    const reader = new FileReader();
    const [file] = target.files;
    setIsImageError(false);
    setIsUploading(true);
    reader.onload = () => {
      setImage({ id: null, link: reader.result });
      setIsUploading(false);
    };
    reader.onerror = () => {
      setIsUploading(false);
      setIsImageError(true);
    };
    reader.readAsDataURL(file);
  };

  const handleProfileUpdate = async () => {
    const isImageChange = image?.id !== user.image?.id;
    const dataToUpdate = { username, status };
    const onFinish = () => {
      setIsUploading(false);
      loadUserPosts();
      setIsEdited(false);
    };

    if (isImageChange) {
      const croppedImage = await getCroppedImage(image?.link ?? DEFAULT_USER_AVATAR, crop);
      setIsUploading(true);
      uploadImage(croppedImage)
        .then(({ id, link }) => {
          setImage({ id, link });
          Object.assign(dataToUpdate, { imageId: id });
          updateCurrentUser(dataToUpdate);
        })
        .catch(() => {
          setIsImageError(true);
        })
        .finally(onFinish);
    } else {
      updateCurrentUser(dataToUpdate);
      onFinish();
    }
  };

  const handleProfileSave = () => {
    const isUsernameChange = username !== user.username;
    const isStatusChange = status !== user.status;
    const isImageChange = image?.id !== user.image?.id;

    if (!isUsernameChange && !isStatusChange && !isImageChange) {
      handleIsEditedToggle();
      return;
    }
    if (isUsernameChange) {
      getUserByUsername()
        .then(({ id: userId } = {}) => {
          if (userId && userId !== user.id) {
            setIsUsernameError(true);
          } else {
            handleProfileUpdate();
          }
        });
    } else {
      handleProfileUpdate();
    }
  };

  React.useEffect(() => {
    setStatus(user.status);
  }, [user]);

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Form onSubmit={handleProfileSave} className={styles.form}>
          <Button
            type={ButtonType.BUTTON}
            color={ButtonColor.BLUE}
            className={clsx(styles.formBtn, isEdited && styles.hidden)}
            onClick={handleIsEditedToggle}
          >
            Edit profile
          </Button>
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.BLUE}
            className={clsx(styles.formBtn, !isEdited && styles.hidden)}
            isDisabled={isUploading || !username}
            isLoading={isUploading}
          >
            Save profile
          </Button>
          <Image
            src={image?.link ?? DEFAULT_USER_AVATAR}
            style={{ width: USER_IMAGE_MAX_SIZE }}
            className={clsx(styles.profileImg, showCrop && styles.hidden)}
          />
          <ReactCrop
            src={image?.link ?? DEFAULT_USER_AVATAR}
            crop={crop}
            locked
            onChange={c => setCrop(c)}
            className={clsx(styles.crop, !showCrop && styles.hidden)}
            imageStyle={{
              minWidth: USER_IMAGE_MAX_SIZE,
              minHeight: USER_IMAGE_MAX_SIZE,
              objectFit: 'none',
              objectPosition: 'top left'
            }}
          />
          <Button
            color="teal"
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
            className={clsx(styles.imgBtn, !isEdited && styles.hidden)}
          >
            <label className={styles.imgBtnLabel}>
              Change image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
          {isImageError && isEdited && (
            <Message compact negative>Failed to upload image</Message>
          )}
          <Form.Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            value={username}
            readOnly={!isEdited}
            className={styles.username}
            onChange={handleUsernameChange}
            error={isUsernameError && {
              content: 'The username is already taken!',
              pointing: 'below'
            }}
          />
          <Form.Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            readOnly
            disabled={isEdited}
            value={user.email}
          />
          <Form.Input
            icon="star"
            iconPosition="left"
            placeholder="Status"
            type="text"
            readOnly={!isEdited}
            onChange={handleStatusChange}
            value={status}
          />
        </Form>
      </Grid.Column>
    </Grid>
  );
};

export default Profile;
