import * as React from 'react';
import clsx from 'clsx';
import { Dropdown as DropdownUI } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { IconName } from 'src/common/enums/enums';
import { Label } from 'src/components/common/common';

import styles from './styles.module.scss';

const Dropdown = ({
  className,
  isFloating,
  direction,
  iconName,
  items
}) => (
  <Label
    basic
    size="small"
    as="a"
    className={clsx(styles.label, className)}
  >
    <DropdownUI
      floating={isFloating}
      direction={direction}
      icon={iconName}
    >
      <DropdownUI.Menu>
        {
          items.map(item => (
            <DropdownUI.Item
              key={item.text}
              text={item.text}
              icon={item.iconName}
              onClick={item.onClick}
            />
          ))
        }
      </DropdownUI.Menu>
    </DropdownUI>
  </Label>
);

Dropdown.propTypes = {
  className: PropTypes.string,
  iconName: PropTypes.oneOf(Object.values(IconName)),
  isFloating: PropTypes.bool,
  direction: PropTypes.string,
  items: PropTypes.arrayOf(
    PropTypes.exact({
      text: PropTypes.string.isRequired,
      iconName: PropTypes.oneOf(Object.values(IconName)),
      onClick: PropTypes.func
    })
  ).isRequired
};

Dropdown.defaultProps = {
  className: null,
  iconName: IconName.ELLIPSIS_VERTICAL,
  isFloating: true,
  direction: 'left'
};

export default Dropdown;
