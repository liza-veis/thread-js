import { ApiPath, AuthApiPath } from '../enums/enums';

const WHITE_ROUTES = [
  `${ApiPath.AUTH}${AuthApiPath.LOGIN}`,
  `${ApiPath.AUTH}${AuthApiPath.REGISTER}`,
  `${ApiPath.AUTH}${AuthApiPath.FORGOT}`,
  `${ApiPath.AUTH}${AuthApiPath.RESET}`
];

export { WHITE_ROUTES };
