import { UsersApiPath } from '../../common/enums/enums';

const initUser = (Router, services) => {
  const { user: userService } = services;
  const router = Router();

  router
    .get(UsersApiPath.USERNAME, (req, res, next) => userService
      .getUserByUsername(req.params.username)
      .then(data => res.send(data || {}))
      .catch(next));

  return router;
};

export { initUser };
