const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  FORGOT: '/forgot',
  RESET: '/reset/:userId/:token',
  PROFILE: '/profile',
  SHARE_$POSTHASH: '/share/:postHash'
};

export { AppRoute };
