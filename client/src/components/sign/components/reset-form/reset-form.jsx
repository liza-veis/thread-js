import * as React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  Message,
  NavLink
} from 'src/components/common/common';

import styles from './styles.module.scss';

const ResetForm = ({ onResetPassword }) => {
  const [password, setPassword] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [isPasswordValid, setIsPasswordValid] = React.useState(true);
  const [isPasswordError, setIsPasswordError] = React.useState(false);
  const history = useHistory();

  const passwordChanged = data => {
    setIsPasswordError(false);
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleResetPasswordClick = () => {
    if (!isPasswordValid || isLoading) {
      return;
    }
    setIsPasswordError(false);
    setIsLoading(true);

    onResetPassword({ password })
      .then(() => {
        history.push(AppRoute.LOGIN);
      })
      .catch(() => {
        setIsPasswordError(true);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <>
      <h2 className={styles.title}>Enter new password</h2>
      <Form name="resetForm" size="large" onSubmit={handleResetPasswordClick}>
        <Segment>
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            error={!isPasswordValid || isPasswordError}
            onChange={ev => passwordChanged(ev.target.value)}
            onBlur={() => setIsPasswordValid(Boolean(password))}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            isLoading={isLoading}
            isFluid
            isPrimary
          >
            Reset password
          </Button>
        </Segment>
      </Form>
      <Message>
        New to us?
        {' '}
        <NavLink exact to={AppRoute.REGISTRATION}>
          Sign Up
        </NavLink>
      </Message>
    </>
  );
};

ResetForm.propTypes = {
  onResetPassword: PropTypes.func.isRequired
};

export default ResetForm;
