const IconName = {
  USER_CIRCLE: 'user circle',
  LOG_OUT: 'log out',
  THUMBS_UP: 'thumbs up',
  THUMBS_DOWN: 'thumbs down',
  COMMENT: 'comment',
  SHARE_ALTERNATE: 'share alternate',
  FROWN: 'frown',
  IMAGE: 'image',
  COPY: 'copy',
  EDIT: 'edit',
  DELETE: 'delete',
  ELLIPSIS_VERTICAL: 'ellipsis vertical',
  PENCIL: 'pencil',
  CHECK: 'check'
};

export { IconName };
