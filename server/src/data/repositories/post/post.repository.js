import { sequelize } from '../../db/connection';
import { Abstract } from '../abstract/abstract.repository';

const likeCase = bool => `(SELECT COUNT(*)
                          FROM "postReactions" as "reaction"
                          WHERE "post"."id" = "reaction"."postId"
                          AND "reaction"."isLike" = ${bool})`;

class Post extends Abstract {
  constructor({
    postModel,
    commentModel,
    userModel,
    imageModel,
    postReactionModel
  }) {
    super(postModel);
    this._commentModel = commentModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._postReactionModel = postReactionModel;
  }

  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      reactionUserId,
      isLike,
      userId
    } = filter;

    const where = {};
    if (userId) {
      Object.assign(where, { userId });
    }

    const reactionWhere = {};
    if (reactionUserId) {
      Object.assign(reactionWhere, { userId: reactionUserId });
    }
    if (['true', 'false'].includes(isLike)) {
      Object.assign(reactionWhere, { isLike: isLike === 'true' });
    }
    const isReactionRequired = Boolean(Object.keys(reactionWhere).length);

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.literal(likeCase(true)), 'likeCount'],
          [sequelize.literal(likeCase(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._userModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._postReactionModel,
        attributes: ['id', 'isLike'],
        duplicating: false,
        where: reactionWhere,
        required: isReactionRequired,
        include: {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id',
        'postReactions.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'postReactions.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.literal(likeCase(true)), 'likeCount'],
          [sequelize.literal(likeCase(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: this._commentModel,
        include: {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }
      }, {
        model: this._userModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._postReactionModel,
        attributes: ['id', 'isLike'],
        duplicating: false,
        include: {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }
      }]
    });
  }
}

export { Post };
