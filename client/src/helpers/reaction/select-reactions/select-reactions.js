const selectReactions = ({ reactions, isLike, count }) => {
  const selected = [];
  for (let i = 0; i < reactions.length; i += 1) {
    if (selected.length >= count) {
      break;
    }
    if (reactions[i].isLike === isLike) {
      selected.push(reactions[i]);
    }
  }

  return selected;
};

export { selectReactions };
