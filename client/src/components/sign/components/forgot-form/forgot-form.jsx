import * as React from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  Message,
  NavLink
} from 'src/components/common/common';
import styles from './styles.module.scss';

const ForgotForm = ({ onResetPasswordRequest }) => {
  const [email, setEmail] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [isEmailValid, setIsEmailValid] = React.useState(true);
  const [isResetPasswordError, setIsResetPasswordError] = React.useState(false);
  const [isSuccess, setIsSuccess] = React.useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
    setIsResetPasswordError(false);
    setIsSuccess(false);
  };

  const handleSendMailClick = () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);
    setIsSuccess(false);
    setIsResetPasswordError(false);

    onResetPasswordRequest({ email })
      .then(() => {
        setIsSuccess(true);
      })
      .catch(() => {
        setIsResetPasswordError(true);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <>
      <h2 className={styles.title}>Enter your email</h2>
      <Form name="forgotForm" size="large" onSubmit={handleSendMailClick}>
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            error={!isEmailValid || isResetPasswordError}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            isLoading={isLoading}
            isFluid
            isPrimary
          >
            Send mail
          </Button>
        </Segment>
      </Form>
      <Message>
        New to us?
        {' '}
        <NavLink exact to={AppRoute.REGISTRATION}>
          Sign Up
        </NavLink>
      </Message>
      {
        isSuccess && (
          <Message positive>
            Message sent successfuly!
          </Message>
        )
      }
    </>
  );
};

ForgotForm.propTypes = {
  onResetPasswordRequest: PropTypes.func.isRequired
};

export default ForgotForm;
