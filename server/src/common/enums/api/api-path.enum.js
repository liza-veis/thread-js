const ApiPath = {
  AUTH: '/auth',
  USERS: '/users',
  POSTS: '/posts',
  COMMENTS: '/comments',
  IMAGES: '/images'
};

export { ApiPath };
