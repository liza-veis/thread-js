import passport from 'passport';

const forgotPasswordAuthentication = passport.authenticate('forgot-password', { session: false });

export { forgotPasswordAuthentication };
