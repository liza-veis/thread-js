import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime, selectReactions } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, Dropdown, ReactionPopup } from 'src/components/common/common';

import styles from './styles.module.scss';

const Post = ({
  post,
  isUserPost,
  onPostLike,
  onPostDislike,
  onPostDelete,
  onExpandedPostToggle,
  onEditedPostToggle,
  sharePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    postReactions
  } = post;
  const [reactions, setReactions] = React.useState([]);
  const date = getFromNowTime(createdAt);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handlePostDelete = () => onPostDelete(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleEditedPostToggle = () => onEditedPostToggle(id);

  React.useEffect(() => {
    const selectedReactions = selectReactions({
      reactions: postReactions,
      isLike: true,
      count: 5
    });
    setReactions(selectedReactions);
  }, [post, postReactions]);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <ReactionPopup reactions={reactions}>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handlePostLike}
          >
            <Icon name={IconName.THUMBS_UP} />
            {likeCount}
          </Label>
        </ReactionPopup>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        {
          isUserPost && (
            <Dropdown
              className={styles.dropdown}
              items={[
                {
                  text: 'Edit post',
                  iconName: IconName.EDIT,
                  onClick: handleEditedPostToggle
                },
                {
                  text: 'Delete post',
                  iconName: IconName.DELETE,
                  onClick: handlePostDelete
                }
              ]}
            />
          )
        }
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  isUserPost: PropTypes.bool.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  onEditedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
