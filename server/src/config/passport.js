import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { ENV } from '../common/enums/enums';
import { user as userRepository } from '../data/repositories/repositories';
import { cryptCompare } from '../helpers/helpers';

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: ENV.JWT.SECRET
};

const resetPasswordOptions = {
  jwtFromRequest: req => req.params.token,
  secretOrKey: ENV.JWT.SECRET,
  passReqToCallback: true
};

passport.use(
  'login',
  new LocalStrategy({ usernameField: 'email' }, async (email, password, done) => {
    try {
      const user = await userRepository.getByEmail(email);
      if (!user) {
        return done({ status: 401, message: 'Incorrect email.' }, false);
      }

      return await cryptCompare(password, user.password)
        ? done(null, user)
        : done({ status: 401, message: 'Passwords do not match.' }, null);
    } catch (err) {
      return done(err);
    }
  })
);

passport.use(
  'register',
  new LocalStrategy(
    { passReqToCallback: true },
    async ({ body: { email } }, username, password, done) => {
      try {
        const userByEmail = await userRepository.getByEmail(email);
        if (userByEmail) {
          return done({ status: 401, message: 'Email is already taken.' }, null);
        }

        return await userRepository.getByUsername(username)
          ? done({ status: 401, message: 'Username is already taken.' }, null)
          : done(null, { email, username, password });
      } catch (err) {
        return done(err);
      }
    }
  )
);

passport.use(
  'forgot-password',
  new LocalStrategy(
    { usernameField: 'email', passwordField: 'email' },
    async (email, password, done) => {
      try {
        const user = await userRepository.getByEmail(email);
        if (!user) {
          return done({ status: 401, message: 'Incorrect email.' }, false);
        }
        return await done(null, user);
      } catch (err) {
        return done(err);
      }
    }
  )
);

passport.use(
  'reset-password',
  new JwtStrategy(resetPasswordOptions, async (req, payload, done) => {
    try {
      const user = await userRepository.getById(req.params.userId);
      return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
    } catch (err) {
      return done(err);
    }
  })
);

passport.use(new JwtStrategy(options, async ({ id }, done) => {
  try {
    const user = await userRepository.getById(id);
    return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
  } catch (err) {
    return done(err);
  }
}));
