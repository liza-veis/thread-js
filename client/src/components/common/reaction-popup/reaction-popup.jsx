import * as React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Popup as PopupUI } from 'semantic-ui-react';
import { reactionType } from 'src/common/prop-types/prop-types';
import { PopupSize } from 'src/common/enums/enums';
import { Image } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';

import styles from './styles.module.scss';

const ReactionPopup = ({
  className,
  size,
  reactions,
  children
}) => {
  if (!reactions.length) {
    return children;
  }

  return (
    <PopupUI
      className={className}
      trigger={children}
      size={size}
    >
      {
        reactions.map(({ user }) => (
          <div key={user.id} className={clsx(styles.item, className)}>
            <Image
              circular
              width="25"
              height="25"
              src={user.image?.link ?? DEFAULT_USER_AVATAR}
            />
            {user.username}
          </div>
        ))
      }
    </PopupUI>
  );
};

ReactionPopup.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  size: PropTypes.oneOf(Object.values(PopupSize)),
  reactions: PropTypes.arrayOf(reactionType).isRequired
};

ReactionPopup.defaultProps = {
  children: null,
  className: null,
  size: PopupSize.SMALL
};

export default ReactionPopup;
