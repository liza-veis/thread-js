import { createReducer } from '@reduxjs/toolkit';
import {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  setEditedPost,
  removePost
} from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  editedPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
  builder.addCase(removePost, (state, action) => {
    const { postId } = action.payload;
    const { posts, expandedPost } = state;

    state.posts = posts.filter(post => post.id !== postId);
    if (expandedPost?.id === postId) {
      state.expandedPost = undefined;
    }
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(setEditedPost, (state, action) => {
    const { post } = action.payload;

    state.editedPost = post;
  });
});

export { reducer };
