import { HttpMethod } from 'src/common/enums/enums';

class User {
  constructor({ http }) {
    this._http = http;
  }

  getUserByUsername(username) {
    const encodedUsername = encodeURIComponent(username);

    return this._http.load(`/api/users/${encodedUsername}`, {
      method: HttpMethod.GET
    });
  }
}

export { User };
