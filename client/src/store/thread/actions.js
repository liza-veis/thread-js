import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  REMOVE_POST: 'thread/remove-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  SET_EDITED_POST: 'thread/set-edited-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const removePost = createAction(ActionType.REMOVE_POST, postId => ({
  payload: {
    postId
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const setEditedPost = createAction(ActionType.SET_EDITED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const loadPost = postId => async (dispatch, getRootState) => {
  const updatedPost = await postService.getPost(postId);
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : updatedPost));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(updatedPost));
  }

  dispatch(setPosts(updated));
};

const loadUserPosts = userId => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();

  posts.forEach(post => {
    if (post.userId === userId) {
      dispatch(loadPost(post.id));
    }
  });
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updatePost = (postId, data) => async dispatch => {
  await postService.updatePost(postId, data);
  dispatch(loadPost(postId));
};

const deletePost = postId => async dispatch => {
  const success = await postService.deletePost(postId);
  if (success) {
    dispatch(removePost(postId));
  }
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const toggleEditedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditedPost(post));
};

const likePost = postId => async dispatch => {
  await postService.likePost(postId);
  dispatch(loadPost(postId));
};

const dislikePost = postId => async dispatch => {
  await postService.dislikePost(postId);
  dispatch(loadPost(postId));
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  removePost,
  updatePost,
  deletePost,
  setExpandedPost,
  setEditedPost,
  loadPosts,
  loadMorePosts,
  loadUserPosts,
  loadPost,
  applyPost,
  createPost,
  toggleExpandedPost,
  toggleEditedPost,
  likePost,
  dislikePost,
  addComment
};
