import passport from 'passport';

const resetPasswordAuthentication = passport.authenticate('reset-password', { session: false });

export { resetPasswordAuthentication };
