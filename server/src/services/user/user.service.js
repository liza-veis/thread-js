class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  async getUserByUsername(username) {
    const user = await this._userRepository.getByUsername(username);

    return user;
  }

  async update(id, data) {
    const user = await this._userRepository.updateById(id, data);

    return user;
  }
}

export { User };
