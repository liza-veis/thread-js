import { matchPath } from 'react-router-dom';
import { HttpMethod, ContentType, AppRoute } from 'src/common/enums/enums';

class Auth {
  constructor({ http }) {
    this._http = http;
  }

  login(payload) {
    return this._http.load('/api/auth/login', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load('/api/auth/register', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  getCurrentUser() {
    return this._http.load('/api/auth/user', {
      method: HttpMethod.GET
    });
  }

  updateCurrentUser(payload) {
    return this._http.load('/api/auth/user', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  requestResetPassword(payload) {
    return this._http.load('/api/auth/forgot', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  resetPassword(payload) {
    const { userId, token } = matchPath(window.location.pathname, AppRoute.RESET).params;
    return this._http.load(`/api/auth/reset/${userId}/${token}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }
}

export { Auth };
