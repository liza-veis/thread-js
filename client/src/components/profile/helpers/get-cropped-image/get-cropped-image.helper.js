const getCroppedImage = (url, crop) => {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');
  const image = new Image();

  canvas.width = crop.width;
  canvas.height = crop.height;
  image.crossOrigin = 'anonymous';

  const handleImageLoad = async () => {
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );
  };

  return new Promise(resolve => {
    image.onload = () => {
      handleImageLoad()
        .then(() => {
          canvas.toBlob(blob => resolve(blob), 'image/jpeg', 1);
        });
    };
    image.src = url;
  });
};

export { getCroppedImage };
