import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Modal, Button, Form, Image, Message } from 'src/components/common/common';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';

import styles from './styles.module.scss';

const EditedPost = ({ onPostUpdate, uploadImage }) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.editedPost
  }));
  const [body, setBody] = React.useState(post?.body || '');
  const [image, setImage] = React.useState(post?.image);
  const [isUploading, setIsUploading] = React.useState(false);
  const [isImageError, setIsImageError] = React.useState(false);

  const handleEditedPostToggle = React.useCallback(id => {
    setIsImageError(false);
    dispatch(threadActionCreator.toggleEditedPost(id));
  }, [dispatch]);

  const handleEditedPostSave = async () => {
    if (body !== post.body || post.image?.id !== image?.id) {
      onPostUpdate(post.id, { imageId: image?.id, body });
    }
    handleEditedPostToggle();
    setBody('');
    setImage(undefined);
    setIsImageError(false);
  };

  const handleUploadFile = ({ target }) => {
    setIsImageError(false);
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
      })
      .catch(() => {
        setIsImageError(true);
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  const handleEditedPostClose = () => handleEditedPostToggle();

  return (
    <Modal
      dimmer="blurring"
      size="small"
      open
      onClose={handleEditedPostClose}
    >
      <Modal.Header className={styles.header}>
        <span>Edit Post</span>
        <Button
          color={ButtonColor.BLUE}
          type={ButtonType.SUBMIT}
          isDisabled={!body || isUploading}
          formId="EditPostForm"
        >
          Save
        </Button>
      </Modal.Header>
      {post ? (
        <Modal.Content>
          <Form onSubmit={handleEditedPostSave} id="EditPostForm">
            <Form.TextArea
              name="body"
              value={body}
              placeholder="What is the news?"
              onChange={ev => setBody(ev.target.value)}
            />
            {image?.imageLink && (
              <>
                <div className={styles.imageWrapper}>
                  <Image className={styles.image} src={image?.imageLink} alt="post" />
                </div>
                {isImageError && (
                  <Message compact negative>Failed to upload image</Message>
                )}
              </>
            )}
            <div className={styles.btnWrapper}>
              <Button
                color={ButtonColor.TEAL}
                isLoading={isUploading}
                isDisabled={isUploading}
                iconName={IconName.IMAGE}
              >
                <label className={styles.btnImgLabel}>
                  Attach image
                  <input
                    name="image"
                    type="file"
                    onChange={handleUploadFile}
                    hidden
                  />
                </label>
              </Button>
            </div>
          </Form>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

EditedPost.propTypes = {
  onPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default EditedPost;
