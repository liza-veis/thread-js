import * as React from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { StorageKey, AppRoute } from 'src/common/enums/enums';
import { storage } from 'src/services/services';
import { profileActionCreator, threadActionCreator } from 'src/store/actions';
import {
  Spinner,
  Header,
  PrivateRoute,
  PublicRoute,
  Notifications
} from 'src/components/common/common';
import SignPage from 'src/components/sign/sign';
import NotFoundPage from 'src/components/not-found/not-found';
import ProfilePage from 'src/components/profile/profile';
import SharedPostPage from 'src/components/shared-post/shared-post';
import ThreadPage from 'src/components/thread/thread';

const signRoutes = [
  AppRoute.LOGIN,
  AppRoute.REGISTRATION,
  AppRoute.FORGOT,
  AppRoute.RESET
];

const Routing = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const dispatch = useDispatch();
  const history = useHistory();
  const [isError, setIsError] = React.useState(false);

  const hasToken = Boolean(storage.getItem(StorageKey.TOKEN));
  const hasUser = Boolean(user);

  const handlePostApply = React.useCallback(id => (
    dispatch(threadActionCreator.applyPost(id))
  ), [dispatch]);

  const handlePostUpdate = React.useCallback(id => (
    dispatch(threadActionCreator.loadPost(id))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.removePost(id))
  ), [dispatch]);

  const handleUserLogout = React.useCallback(() => (
    dispatch(profileActionCreator.logout())
  ), [dispatch]);

  React.useEffect(() => {
    setIsError(false);
    if (hasToken) {
      dispatch(profileActionCreator.loadCurrentUser()).catch(() => {
        setIsError(true);
        storage.setItem(StorageKey.TOKEN, null);
        history.push(AppRoute.LOGIN);
      });
    }
  }, [hasToken, dispatch, history]);

  if (!hasUser && hasToken && !isError) {
    return <Spinner isOverflow />;
  }

  return (
    <div className="fill">
      {hasUser && (
        <header>
          <Header user={user} onUserLogout={handleUserLogout} />
        </header>
      )}
      <main className="fill">
        <Switch>
          <PublicRoute exact path={signRoutes} component={SignPage} />
          <PrivateRoute exact path={AppRoute.ROOT} component={ThreadPage} />
          <PrivateRoute exact path={AppRoute.PROFILE} component={ProfilePage} />
          <PrivateRoute path={AppRoute.SHARE_$POSTHASH} component={SharedPostPage} />
          <Route path={AppRoute.ANY} exact component={NotFoundPage} />
        </Switch>
      </main>
      <Notifications
        onPostApply={handlePostApply}
        onPostUpdate={handlePostUpdate}
        onPostDelete={handlePostDelete}
        user={user}
      />
    </div>
  );
};

export default Routing;
