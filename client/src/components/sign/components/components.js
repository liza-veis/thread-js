import LoginForm from './login-form/login-form';
import RegistrationForm from './registration-form/registration-form';
import ForgotForm from './forgot-form/forgot-form';
import ResetForm from './reset-form/reset-form';

export { LoginForm, RegistrationForm, ForgotForm, ResetForm };
