import * as React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { profileActionCreator, threadActionCreator } from 'src/store/actions';
import { IconName, IconSize, ButtonType, AppRoute } from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { userType } from 'src/common/prop-types/prop-types';
import {
  Button,
  Icon,
  Image,
  Grid,
  NavLink,
  Label,
  Input
} from 'src/components/common/common';

import styles from './styles.module.scss';

const Header = ({ user, onUserLogout }) => {
  const [status, setStatus] = React.useState(user.status);
  const [isStatusEdited, setIsStatusEdited] = React.useState(false);
  const dispatch = useDispatch();

  const loadUserPosts = () => (
    dispatch(threadActionCreator.loadUserPosts(user.id))
  );

  const handleStatusSave = () => {
    dispatch(profileActionCreator.updateCurrentUser({ status }));
  };

  const handleIsStatusEditedToggle = () => {
    if (isStatusEdited && status !== user.status) {
      handleStatusSave();
      loadUserPosts();
    }
    setIsStatusEdited(!isStatusEdited);
  };

  const handleStatusChange = ev => {
    setStatus(ev.target.value);
  };

  React.useEffect(() => {
    setStatus(user.status);
  }, [user]);

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <div className={styles.userWrapper}>
              <NavLink exact to={AppRoute.ROOT}>
                <Image
                  circular
                  width="45"
                  height="45"
                  src={user.image?.link ?? DEFAULT_USER_AVATAR}
                />
              </NavLink>
              <div>
                <NavLink
                  exact
                  to={AppRoute.ROOT}
                  style={{ color: 'black' }}
                >
                  {user.username}
                </NavLink>
                <div className={styles.userStatus}>
                  <Input
                    size="small"
                    value={status}
                    placeholder="Add a status"
                    readOnly={!isStatusEdited}
                    className={styles.statusInput}
                    onChange={handleStatusChange}
                  />
                  <Label
                    basic
                    as="a"
                    className={styles.statusLabel}
                    onClick={handleIsStatusEditedToggle}
                  >
                    <Icon name={isStatusEdited ? IconName.CHECK : IconName.PENCIL} />
                  </Label>
                </div>
              </div>
            </div>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink
            exact
            activeClassName="active"
            to={AppRoute.PROFILE}
            className={styles.menuBtn}
          >
            <Icon name={IconName.USER_CIRCLE} size={IconSize.LARGE} />
          </NavLink>
          <Button
            className={clsx(styles.menuBtn, styles.logoutBtn)}
            onClick={onUserLogout}
            type={ButtonType.BUTTON}
            iconName={IconName.LOG_OUT}
            iconSize={IconSize.LARGE}
            isBasic
          />
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  onUserLogout: PropTypes.func.isRequired,
  user: userType.isRequired
};

export default Header;
