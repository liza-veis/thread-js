import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, EditedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  reactionUserId: undefined,
  isLike: undefined
};

const Thread = () => {
  const {
    posts,
    hasMorePosts,
    expandedPost,
    editedPost,
    userId
  } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    editedPost: state.posts.editedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showLikedPosts, setShowLikedPosts] = React.useState(false);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => {
    dispatch(threadActionCreator.likePost(id));
    if (showLikedPosts) {
      dispatch(threadActionCreator.removePost(id));
    }
  }, [dispatch, showLikedPosts]);

  const handlePostDislike = React.useCallback(id => {
    dispatch(threadActionCreator.dislikePost(id));
    if (showLikedPosts) {
      dispatch(threadActionCreator.removePost(id));
    }
  }, [dispatch, showLikedPosts]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleEditedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleEditedPost(id))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostUpdate = React.useCallback((id, data) => (
    dispatch(threadActionCreator.updatePost(id, data))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(postPayload => (
    dispatch(threadActionCreator.deletePost(postPayload))
  ), [dispatch]);

  const handlePostsLoad = React.useCallback(() => {
    postsFilter.from = 0;
    dispatch(threadActionCreator.loadPosts(postsFilter));
    postsFilter.from = postsFilter.count;
  }, [dispatch]);

  const handleMorePostsLoad = () => {
    dispatch(threadActionCreator.loadMorePosts(postsFilter));
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    handlePostsLoad();
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.reactionUserId = showLikedPosts ? undefined : userId;
    postsFilter.isLike = showLikedPosts ? undefined : true;
    handlePostsLoad();
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  React.useEffect(() => {
    if (!expandedPost && showLikedPosts) {
      handlePostsLoad();
    }
  }, [expandedPost, showLikedPosts, handlePostsLoad]);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Liked by me"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={handleMorePostsLoad}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            isUserPost={userId === post.userId}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onPostDelete={handlePostDelete}
            onExpandedPostToggle={handleExpandedPostToggle}
            onEditedPostToggle={handleEditedPostToggle}
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {editedPost && <EditedPost onPostUpdate={handlePostUpdate} uploadImage={uploadImage} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
