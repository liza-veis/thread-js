import { encrypt, createToken, sendResetPasswordMail } from '../../helpers/helpers';
import { ENV } from '../../common/enums/enums';

const { URL: CLIENT_URL } = ENV.CLIENT;

class Auth {
  constructor({ userRepository }) {
    this._userRepository = userRepository;

    this.register = this.register.bind(this);
  }

  async login({ id }) {
    return {
      token: createToken({ id }),
      user: await this._userRepository.getUserById(id)
    };
  }

  async register({ password, ...userData }) {
    const newUser = await this._userRepository.addUser({
      ...userData,
      password: await encrypt(password)
    });

    return this.login(newUser);
  }

  async requestResetPassword({ email }) {
    const user = await this._userRepository.getByEmail(email);
    const { id } = user;
    const token = createToken({ id });
    const link = `${CLIENT_URL}/reset/${id}/${token}`;

    await sendResetPasswordMail({ to: email, link });
    return { id };
  }

  async resetPassword(id, { password }) {
    const hashedPassword = await encrypt(password);
    const user = await this._userRepository.updateById(id, { password: hashedPassword });

    return user;
  }
}

export { Auth };
